# AP-II FastFood

Projeto feito para a disciplina de algorítimos de programação II

Ideia: Um sistema simples que não é necessario manter funcionario/cliente, um sistema de balcao onde ele possa realizar o pedido diretamente e ter o
controle de estoque (quantidade) dos produtos.


*Trabalho:
Desenvolver uma aplicação com os conceitos trabalhados no semestre e utilização de novas tecnologias.


Regras:

O trabalho poderá ser realizado em grupos de três integrantes.

A aplicação deverá rodar com um banco de dados ou pelo menos armazenamento em arquivo.

A linguagem para desenvolvimento deverá ser java.

Todos os conceitos estudados devem estar evidenciados no desenvolvimento do trabalho: Boas práticas de O.O, tratamento de exceção, encapsulamento.


Tarefas:

Desenvolver uma aplicação pequena, visando um negócio ou parte de um. Ex.: Controle do estoque do boteco da esquina, alguma aplicação projetada em outra unidade curricular.

Desenvolver o diagrama ER da aplicação.

Levantar os requisitos funcionais e não-funcionais da aplicação.